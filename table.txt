| Name | Type | Arm length [m]| Incident angle [deg]| klog|
| ---- | ---- | ---- | ----| ----|
|MCE_TM_OPLEV_TILT   | REGULAR | 0.95 |36.5|['3175', '3721']|
|MCI_TM_OPLEV_TILT   | REGULAR | 0.86 |0.23|['3175', '3721']|
|MCO_TM_OPLEV_TILT   | FOLDED | 0.85 |17.8|['3175', '3721']|
|IMMT1_TM_OPLEV_TILT | --- | --- |---|['']|
|IMMT2_TM_OPLEV_TILT | --- | [---, ---] |---|['']|
|PR2_TM_OPLEV_TILT   | --- | --- |---|['']|
|PR2_TM_OPLEV_LEN    | --- | [---, ---] |---|['']|
|PR3_TM_OPLEV_TILT   | REGULAR | 0.8 |45|['']|
|PR3_TM_OPLEV_LEN    | REGULAR | [0.8, ---] |45|['']|
|PRM_TM_OPLEV_TILT   | --- | --- |---|['']|
|PRM_TM_OPLEV_LEN    | --- | [---, ---] |---|['']|
|SR2_TM_OPLEV_TILT   | --- | --- |---|['']|
|SR2_TM_OPLEV_LEN    | --- | [---, ---] |---|['']|
|SR3_TM_OPLEV_TILT   | --- | --- |---|['']|
|SR3_TM_OPLEV_LEN    | --- | [---, ---] |---|['']|
|SRM_TM_OPLEV_TILT   | --- | --- |---|['']|
|SRM_TM_OPLEV_LEN    | --- | [---, ---] |---|['']|
|BS_TM_OPLEV_TILT    | --- | --- |---|['']|
|BS_TM_OPLEV_LEN     | --- | [---, ---] |---|['']|
|ETMX_TM_OPLEV_TILT  | REGULAR | 1.856 |31.7|['']|
|ETMX_TM_OPLEV_LEN   | REGULAR | [1.856, ---] |31.7|['']|
|ETMX_MN_OPLEV_TILT  | --- | --- |---|['']|
|ETMX_MN_OPLEV_LEN   | --- | [---, ---] |---|['']|
|ETMX_MN_OPLEV_ROL   | --- | --- |---|['']|
|ETMX_MN_OPLEV_TRA   | --- | --- |---|['']|
|ETMX_MN_OPLEV_VER   | --- | --- |---|['']|
|ETMX_PF_OPLEV_TILT  | --- | --- |---|['']|
|ETMX_PF_OPLEV_LEN   | --- | --- |---|['']|
|ETMY_TM_OPLEV_TILT  | REGULAR | 1.856 |31.7|['']|
|ETMY_TM_OPLEV_LEN   | REGULAR | [1.856, ---] |31.7|['']|
|ETMY_MN_OPLEV_TILT  | --- | --- |---|['']|
|ETMY_MN_OPLEV_LEN   | --- | [---, ---] |---|['']|
|ETMY_MN_OPLEV_ROL   | --- | --- |---|['']|
|ETMY_MN_OPLEV_TRA   | --- | --- |---|['']|
|ETMY_MN_OPLEV_VER   | --- | --- |---|['']|
|ETMY_PF_OPLEV_TILT  | --- | --- |---|['']|
|ETMY_PF_OPLEV_LEN   | --- | --- |---|['']|
|ITMX_TM_OPLEV_TILT  | REGULAR | 1.736 |31.7|['']|
|ITMX_TM_OPLEV_LEN   | REGULAR | [1.636, 0.36] |31.7|['']|
|ITMX_MN_OPLEV_TILT  | REGULAR | 2.219 |60.8|['']|
|ITMX_MN_OPLEV_LEN   | REGULAR | [2.179, 0.335] |60.8|['']|
|ITMX_MN_OPLEV_ROL   | REGULAR | 2.242 |30.5|['']|
|ITMX_MN_OPLEV_TRA   | REGULAR | [2.212, 0.345] |30.5|['']|
|ITMX_MN_OPLEV_VER   | --- | --- |---|['']|
|ITMX_PF_OPLEV_TILT  | --- | --- |---|['']|
|ITMX_PF_OPLEV_LEN   | --- | --- |---|['']|
|ITMY_TM_OPLEV_TILT  | REGULAR | 1.856 |31.7|['']|
|ITMY_TM_OPLEV_LEN   | REGULAR | [1.856, ---] |31.7|['']|
|ITMY_MN_OPLEV_TILT  | --- | --- |---|['']|
|ITMY_MN_OPLEV_LEN   | --- | [---, ---] |---|['']|
|ITMY_MN_OPLEV_ROL   | --- | --- |---|['']|
|ITMY_MN_OPLEV_TRA   | --- | --- |---|['']|
|ITMY_MN_OPLEV_VER   | --- | --- |---|['']|
|ITMY_PF_OPLEV_TILT  | --- | --- |---|['']|
|ITMY_PF_OPLEV_LEN   | --- | --- |---|['']|
